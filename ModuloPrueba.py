'''
Created on 11 mar. 2021

@author: Rafael Villaneda
'''
from tkinter import *
import math
from tkinter import messagebox as msg
class Calculadora:
    def __init__(self):
        self.operacionRealizar = ""
        self.ventana= Tk()
        self.ventana.title("Calculadora")
        
        self.ventana.resizable(0,0)
        
        self.caja = StringVar()
        self.caja.set("0")
        self.__frame = Frame(self.ventana)
        self.__frame.pack()
        
        self.__frame.config(bg="#F0F0F0", cursor="hand2")
        
        # Caja de texto
        self.txtResultado = Entry(self.__frame, font=("bold", 22), justify="right", borderwidth=1, state=DISABLED, textvariable=self.caja).grid(row=0, column=0, columnspan=4)
        
        #------------------------------------------------------------------
        self.btnResiduo = Button(self.__frame, text="%", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("%")).grid(row=1, column=0)
        self.btnRaiz = Button(self.__frame, text="√", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("√")).grid(row=1, column=1)
        self.btnCuadrado = Button(self.__frame, text="x²", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("x²")).grid(row=1, column=2)
        self.btnUnoSobreX = Button(self.__frame, text="1/x", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("1/x")).grid(row=1, column=3)
        
        self.btnCE = Button(self.__frame, text="CE", font=("bold", 15), borderwidth=0, width=7, height=1, background="GRAY", command=self.eliminarCE).grid(row=2, column=0)
        self.btnC = Button(self.__frame, text="C", font=("bold", 15), borderwidth=0, width=7, height=1, background="GRAY", command=self.eliminarC).grid(row=2, column=1)
        self.btnBorrar = Button(self.__frame, text="<-", font=("bold", 15), borderwidth=0, width=7, height=1, background="GRAY", command=self.borrarCaracter).grid(row=2, column=2)
        self.btnDiv = Button(self.__frame, text="/", font=("bold", 15), borderwidth=0, width=7, height=1, background="GRAY", command=lambda:self.realizarOperacion("/")).grid(row=2, column=3)
        
        self.btn7 = Button(self.__frame, text="7", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(7)).grid(row=3, column=0)
        self.btn8 = Button(self.__frame, text="8", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(8)).grid(row=3, column=1)
        self.btn9 = Button(self.__frame, text="9", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(9)).grid(row=3, column=2)
        self.btn10= Button(self.__frame, text="x", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("x")).grid(row=3, column=3)
        
        self.btn4 = Button(self.__frame, text="4", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(4)).grid(row=4, column=0)
        self.btn5 = Button(self.__frame, text="5", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(5)).grid(row=4, column=1)
        self.btn6 = Button(self.__frame, text="6", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(6)).grid(row=4, column=2)
        self.btnMenos = Button(self.__frame, text="-", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("-")).grid(row=4, column=3)
        
        self.btn1 = Button(self.__frame, text="1", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(1)).grid(row=5, column=0)
        self.btn2 = Button(self.__frame, text="2", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(2)).grid(row=5, column=1)
        self.btn3 = Button(self.__frame, text="3", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(3)).grid(row=5, column=2)
        self.btnMas = Button(self.__frame, text="+", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("+")).grid(row=5, column=3)
        
        self.btnMasMenos = Button(self.__frame, text="+-", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=self.cambiarSigno).grid(row=6, column=0)
        self.btn0 = Button(self.__frame, text="0", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.ingresarNum(0)).grid(row=6, column=1)
        self.btnPunto = Button(self.__frame, text=".", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=self.punto).grid(row=6, column=2)
        self.btnIgual = Button(self.__frame, text="=", font=("bold", 15), borderwidth=0, width=7, height=2, background="GRAY", command=lambda:self.realizarOperacion("=")).grid(row=6, column=3)

        
    def residuo(self):
        self.operacion = f"{self.caja.get()}%"
        self.caja.set(self.operacion)
        pass
    def suma(self):
        self.operacion = f"{self.caja.get()}+"
        self.caja.set(self.operacion)
        pass
    def mult(self):
        self.operacion = f"{self.caja.get()}*"
        self.caja.set(self.operacion)
        pass
    def resta(self):
        #self.operacion = f"{self.caja.get()}^2"
        self.operacion=eval(f"pow({self.caja.get()}, 2)")
        self.caja.set(self.operacion)
        pass
    def div(self):
        self.operacion = f"{self.caja.get()}/"
        self.caja.set(self.operacion)
        pass
    def realizarOperacion(self, opcion):
        try:
            
            if opcion=="√":
                self.raiz()
            elif opcion=="x²":
                self.cuadrado()
            elif opcion=="1/x":
                self.unoSobreX()
            elif opcion=="+":
                self.suma()
            elif opcion=="-":
                self.resta()
            elif opcion=="x":
                self.mult()
            elif opcion=="/":
                self.div()
            elif opcion=="=":
                self.igual()
            elif opcion=="%":
                self.residuo()
        
        except NameError as e:
            self.eliminarC()

        except SyntaxError as e:
            self.eliminarC()
        
        except ZeroDivisionError as e:
            self.eliminarC()

        pass
    def cuadrado(self):
        if not self.caja.get()=="0":
            self.operacion=eval(f"pow({self.caja.get()}, 2)")
            self.caja.set(self.operacion)
        pass
    def punto(self):
        self.operacion = f"{self.caja.get()}."
        self.caja.set(self.operacion)
        pass
    def eliminarC(self):
        self.caja.set("0")
        self.operacion = ""
        pass
    def cambiarSigno(self):
        texto=self.caja.get()
        
        if not texto=="0":
            if texto[0]=="-":
                texto=texto.replace(texto[0], "")
            else:
                texto="-"+texto
        
        self.operacion=texto
        self.caja.set(texto)
        pass
    def raiz(self):
        if not self.caja.get()=="0":
            try:
                self.operacion = math.sqrt(eval(f"{self.operacion}"))
                self.caja.set(self.operacion)
            except ValueError as e:
                self.eliminarC()
        pass
    def igual(self):
        self.operacion = eval(self.operacion)
        self.caja.set(self.operacion)
        pass
    def eliminarCE(self):
        self.caja.set("0")
        pass
    def borrarCaracter(self):
        texto = self.caja.get()
        largo = len(texto)
        
        if not largo==1:
            self.caja.set(texto[:largo-1])
        else:
            self.caja.set("0")
        pass
    def unoSobreX(self): 
        self.operacion=eval(f"1/({self.caja.get()})")
        self.caja.set(self.operacion)
        pass
    def ingresarNum(self, num):
        texto = self.caja.get()

        if len(texto)==1 and texto[0]=="0":
            self.operacion=f"{self.caja.get()[1:]}{num}"
        else:
            self.operacion=f"{self.caja.get()}{num}"
            
        self.caja.set(self.operacion)
        pass
    def mostrar(self):
        self.ventana.mainloop()
    
    pass

#------------------------------------

calcu=Calculadora()
calcu.mostrar()()